var server = require('./server');
var expect = require('chai').expect;
var request = require('request');
var portHTTPS = 9001;
var fs = require('fs');
var token = 'Bearer fdsfds';
var data = {};

describe('Start servers', function () {
    it('Should start https server', function (done) {
        server.createServer(portHTTPS, function (result) {
            expect(result).equals(true);
            done();
        });
    });
});

describe('Translate Яблоко весит на дереве', function () {
    it('Should return', function (done) {
        request.post({
            url: 'http://localhost:' + portHTTPS + '/api/translate',
            headers: {
                'authorization': token
            },
            body: {
                from: 'en',
                to: 'es',
                models: 1,
                text: 'I like ice-cream very much.',
                paid: 0,
                platform: 'test_server'
            },
            json: true
        }, function (err, res, body) {
            console.log(body);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Should return House', function (done) {
        request.post({
            url: 'http://localhost:' + portHTTPS + '/api/translate',
            headers: {
                'authorization': token
            },
            body: {
                from: 'ru',
                to: 'en',
                direct: 1,
                text: 'Яблоко висит на дереве. Дом на холме.',
                paid: 0,
                platform: 'test_server'
            },
            json: true
        }, function (err, res, body) {
            console.log(body);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Should return from language', function (done) {
        request.post({
            url: 'http://localhost:' + portHTTPS + '/api/translate',
            headers: {
                'authorization': token
            },
            body: {
                to: 'en',
                direct: 1,
                text: 'Яблоко висит на дереве. Дом на горе.',
                paid: 0,
                platform: 'test_server'
            },
            json: true
        }, function (err, res, body) {
            console.log(body);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Should return from language', function (done) {
        request.post({
            url: 'http://localhost:' + portHTTPS + '/api/translate',
            headers: {
                'authorization': token
            },
            body: {
                to: 'en',
                direct: 1,
                text: 'Яблоко висит на дереве. Hola.',
                paid: 0,
                platform: 'test_server'
            },
            json: true
        }, function (err, res, body) {
            console.log(body);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
    it('Should return from language lang', function (done) {
        request.post({
            url: 'http://localhost:' + portHTTPS + '/api/translate',
            headers: {
                'authorization': token
            },
            body: {
                lang: 'ru-en',
                direct: 1,
                text: 'Яблоко висит на дереве. Hola.',
                paid: 0,
                platform: 'test_server'
            },
            json: true
        }, function (err, res, body) {
            console.log(body);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
});

describe('Translate Яблоко весит на дереве', function () {
    it('Without google and from (single)', function (done) {
        request.post({
            url: 'http://localhost:' + portHTTPS + '/api/multipleTranslate',
            headers: {
                'authorization': token
            },
            body: {
                to: 'en',
                data: ['Яблоко висит на дереве. Дом на холме.']
            },
            json: true
        }, function (err, res, body) {
            console.log(body);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });
});

describe('Close servers', function () {
    it('Should close https server', function () {
        server.closeServer(function (result) {
            expect(result).equals(true);
            done();
        });
    });
});