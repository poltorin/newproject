module.exports = function(id, name, age, email, country, creation_time) {
    var id = id;
    var name = name;
    var age = age;
    var email = email;
    var country = country;
    var creation_time = creation_time;
    
    this.getId = function() {
        return id;
    };
    
    this.setName = function(n) {
        name = n;
    };
    
    this.setAge = function(a) {
        age = a;
    };
    
    this.setEmail = function(e) {
        email = e;
    };
    
    this.setCountry = function(c) {
        country = c;
    };
    
    this.toJSON = function() {
        return {
            id: id,
            name: name,
            age: age,
            email: email,
            country: country,
            creation_time: creation_time
        };
    }
};
