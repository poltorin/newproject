module.exports = function(router) {
    
    var Player = require('./app/models/player');
    
    var apiList = {
        status: 101,
        list: {
            actions: {
                create: {
                    type: 'post',
                    url: '/api/players',
                    params: ['name', 'age', 'email', 'country']
                },
                read: {
                    type: 'get',
                    url: '/api/players',
                    params: []
                },
                read_single: {
                    type: 'get',
                    url: '/api/players/:player_id',
                    params: []
                },
                update: {
                    type: 'put',
                    url: '/api/players/:player_id',
                    params: ['name', 'age', 'email', 'country']
                },
                delete: {
                    type: 'delete',
                    url: '/api/players/:player_id',
                    params: []
                }
            }
        }
    };
    
    var players = [];
    
    var lastInsertId = 0;
    
    router.route('/')
        .get(function(req, res) {
            res.json(apiList);
        });
    
    router.route('/players')
        .get(function(req, res) {
            res.json(players);
        })
        .post(function(req, res) {
            var name = req.body.name;
            var age = req.body.age;
            var email = req.body.email;
            var country = req.body.country;
            
            if (name != null || age != null || email != null || country != null) {
                lastInsertId++;
                
                var player = new Player(lastInsertId, name, age, email, country, new Date().valueOf());
                
                players.push(player);
                
                res.json({status: 101, list: { playerId: lastInsertId }});
            } else {
                res.json({status: 102, message: 'Missing mandatory param'});
            }
        });
    
    router.route('/players/:player_id')
        .get(function(req, res) {
            var find = players.filter(function(item) {
                return item.getId() == req.params.player_id;
            });
            
            if (find.length > 0) {
                var player = find[0];
                res.json({status: 101, list: { player: player }});
            } else {
                res.json({status: 101, list: { message: 'Player not found'}});
            }
        })
        .put(function(req, res) {
            var find = players.filter(function(item) {
                return item.getId() == req.params.player_id;
            });
            
            if (find.length > 0) {
                var player = find[0];
                var name = req.body.name;
                var age = req.body.age;
                var email = req.body.email;
                var country = req.body.country;
                
                if (name != null) player.setName(name);
                if (age != null) player.setAge(age);
                if (email != null) player.setEmail(email);
                if (country != null) player.setCountry(country);
                
                res.json({status: 101, list: { player: player }});
            } else {
                res.json({status: 101, list: { message: 'Player not found'}});
            }
        })
        .delete(function(req, res) {
            var find = players.filter(function(item) {
                return item.getId() == req.params.player_id;
            });
            
            if (find.length > 0) {
                players = players.filter(function(item) {
                    return item.getId() != req.params.player_id;
                });
                
                res.json({status: 101, list: { message: 'Player deleted'}});
            } else {
                res.json({status: 101, list: { message: 'Player not found'}});
            }
        });
}
