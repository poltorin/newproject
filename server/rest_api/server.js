var express     = require('express');
var app         = express();

var path        = require("path");
var bodyParser  = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = 5630;

var router = express.Router();
var routerAPI = express.Router();

router.get('/', function(req, res) {
    res.send('API available <a href="/api">here</a>.'); 
});

require('./routes_api.js')(routerAPI);

app.use('/', router);
app.use('/api', routerAPI);

app.listen(port);
console.log('Server started on port ' + port + '.');
