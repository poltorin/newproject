var express = require('express');
var bodyParser = require('body-parser');
var server;

var createServer= function(port, callback) {
    var app = express();
    app.use(bodyParser.json());


    var port = 5640;
    var users = [
        {
            id: 1,
            userName: "John Lennon",
            profession: "Singer"
        }, {
            id: 2,
            userName: "Larry King",
            profession: "Interviewer"
        }, {
            id: 3,
            userName: "Steve Wozniak",
            profession: "Engineer"
        }
    ];

    app.get('/getUsers', function (req, res) {
        if (users.length)
            res.status(200).json({err: null, result: users});
        res.status(400).json({err: 'Bad data', result: null});
    });

    app.post('/createUser', function (req, res) {
        if (!req.body.userName || !req.body.profession)
            res.status(400).json({err: 'Bad data', result: null});
        let maxId = 0;
        for (let i = 0, len = users.length; i < len; i++) {
            if (maxId < users[i].id)
                maxId = users[i].id;
        }
        users[users.length] = {
            id: maxId + 1,
            userName: req.body.userName,
            profession: req.body.profession
        };
        res.status(200).json(users);
    });

    app.get('/getUser/:id', function (req, res) {
        if (!req.params.id)
            res.status(400).json({err: "user doesn't exists", result: null});
        for (let i = 0, len = users.length; i < len; i++) {
            if (users[i].id == req.params.id)
                res.status(200).json(users[i]);
        }
    });

    app.delete('/deleteUser/:id', function (req, res) {
        if (!req.params.id)
            res.status(400).json({err: "user doesn't exists", result: null});
        for (let i = 0, len = users.length; i < len; i++) {
            if (users[i].id == req.params.id) {
                res.status(200).json({deletedUserId: users[i].id});
                users.splice(i, 1);
            }
        }
    });

    app.put('/updateUser/:id', function (req, res) {
        if (!req.params.id || !req.body.userName || !req.body.profession)
            res.status(400).json({err: 'bad data', result: null});
        for (let i = 0, len = users.length; i < len; i++) {
            if (users[i].id == req.params.id) {
                users[i].userName = req.body.userName;
                users[i].profession = req.body.profession;
                res.status(200).json({updatedUserId: users[i].id});
            }
        }
    });


    app.listen(port, function () {
        console.log("The server is running on port " + port)
    });

    return callback(true);
}


var closeServer = function (callback) {
    server.close();
    return callback(true);
};

module.exports.createServer = createServer;
module.exports.closeServer = closeServer;