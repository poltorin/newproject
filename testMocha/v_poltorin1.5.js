var express = require('express');
var bodyParser = require('body-parser');
var User = require('./user');
var mongoose = require('mongoose');

var index = 0;
var server;
var port = 5640;

var db = mongoose.connect('mongodb://46.101.138.148:5641/test');

mongoose.connection.on('error', function (error) {
    if (error) throw new Error(' no connection with db')
});
mongoose.connection.on('connected', function () {
    console.log('On - Ready state MongoDB  = ' + mongoose.connection.readyState);
});

var createServer = function(port, callback) {
    var app = express();
    app.use(bodyParser.json());

    User.remove({}, () => {});

    app.get('/getUsers', function (req, res) {
        User.find(function(err, users){
            err ?  res.status(400).json({ err: 'DBError', result: null }) : res.status(200).json({ err: null, result : users });
        });
    });

    app.post('/createUser', function (req, res) {
        if (!req.body.userName || !req.body.profession)
            res.status(400).json({err: 'Bad data', result: null});
        index++;
        User.create({
            id: index,
            userName: req.body.userName,
            profession: req.body.profession
        }, function (err, user) {
            err ? res.status(400).json({err: 'DBase Error', result: null}) : res.status(200).json({ err: null, result: user});
        });
    });

    app.get('/getUser/:id', function (req, res) {
        if (!req.params.id)
            return res.status(400).json({err: "user doesn't exist", result: null});
        User.findOne({"id": req.params.id}, (err, user) => {
            !user ? res.status(400).json({err: 'user doesnt exist', result: null}) : res.status(200).json({err: null, result: user});
        });
    });

    app.delete('/deleteUser/:id', function (req, res) {
        if (!req.params.id)
            return res.status(400).json({err: "user doesn't exists", result: null});
        User.remove({
            "id": req.params.id
        },(err, user) => {
            !user ? res.status(400).json({err: 'No such user', result: null}) : res.status(200).json({ err: null, result : 'user' });
        });
    });

    app.put('/updateUser/:id', function (req, res) {
        if (!req.params.id || !req.body.userName || !req.body.profession)
            return res.status(400).json({err: 'bad data', result: null});
        //for (let i = 0, len = users.length; i < len; i++) {
        //    if (users[i].id == req.params.id) {
        //        users[i].userName = req.body.userName;
        //        users[i].profession = req.body.profession;
        //        return res.status(200).json({result: users});
        //    }
        //}
        User.findOneAndUpdate({"id": req.params.id},
            {
                $set: {
                        userName: req.body.userName,
                        profession: req.body.profession
                      }
            },
            (err, user) => {
                !user ? res.status(400).json({err: 'No such user', result: null}) : res.status(200).json({ err: null, result : 'updating completed'});
            }
        );
    });


    server = app.listen(port, function () {
        console.log("The server is running on port " + port)
    });

    return callback(true);
}


var closeServer = function (callback) {
    server.close();
    return callback(true);
};

module.exports.createServer = createServer;
module.exports.closeServer = closeServer;
createServer(5640, () => {});