var server = require('./v_poltorin1.5');
var expect = require('chai').expect;
var request = require('request');
var express = require('express');
var port = 5640;

describe('Start servers', function () {

    it('#Should start http server', function (done) {
        server.createServer(port, function (result) {
            expect(result).equals(true);
            done();
        });
    });
});

describe('Get all users', function () {

    it('#Should return count of users', function (done){
       request.get({
        url: 'http://46.101.138.148:5640/getUsers',
        json: true
        },
        function (err, res, body) {
            expect(res.body.result.length).to.equal(3);
            expect(res.statusCode).to.equal(200);
            done();
        });
    });

    it('#Should return name of 2 user', function (done){
        request.get({
                url: 'http://46.101.138.148:5640/getUsers',
                json: true
            },
            function (err, res, body) {
                expect(res.body.result[1].userName).to.equal("Larry King");
                expect(res.statusCode).to.equal(200);
                done();
            });
    });

    it('#Should return 400 code becouse of bad url', function (done){
        request.get({
                url: 'http://46.101.138.148:5640/getUsers/31',
                json: true
            },
            function (err, res, body) {
                expect(res.statusCode).to.equal(400);
                //expect(res.body.err).to.equal('Bad data');
                done();
            });
    });
});

describe('Creating user', function () {
    it('#Should return count of users', function (done){
        request.post({
                url: 'http://46.101.138.148:5640/createUser',
                body: {
                    userName: 'Vlad',
                    profession: 'student'
                },
                json: true
            },
            function (err, res, body) {
                expect(res.body.result[res.body.result.length-1].userName).to.equal('Vlad');
                expect(res.statusCode).to.equal(200);
                done();
            });
    });
});

describe('Update user', function() {
    it('#update 2 user', function (done){
        request.put({
                url: 'http://46.101.138.148:5640/updateUser/2',
                json: true,
                body: {
                    userName: "Anya",
                    profession: "cosmonaut"
                }
            },
            function (err, res, body) {
                expect(res.body.result[2].userName).to.equal("Anya");
                expect(res.statusCode).to.equal(200);
                done();
            });
    });

    it ('Has to throw 400 and error - no such user exists', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/getUser/51',
            json:true,
        };
        request.get(options, function(error,res,body) {
            expect(res.statusCode).to.equal(400);
            expect(res.body.err).to.equal('Bad data');
            done();
        });
    });
});




describe('http://46.101.138.148:5640/createUser', function() {
    it ('Has to throw 400 and send err:\"Bad data\" - no information has been given', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/createUser',
            json:true,
        };
        request.post(options, function(error,res,body) {
            expect(res.statusCode).to.equal(400);
            expect(res.body.err).to.equal('Bad data');
            done();
        });
    });

    it ('Has to throw 400 and send err:\"Bad data\" - no profession has been given', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/createUser',
            body: {
                "userName": 'vasya',
                "profession": null
            },
            json:true,
        };
        request.post(options, function(error,res,body) {
            expect(res.statusCode).to.equal(400);
            expect(res.body.err).to.equal('Bad data');
            done();
        });
    });

    it ('Has to throw 400 and send err:\"Bad data\" - profession consists of numbers only', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/createUser',
            body: {
                "userName": 'vasya22',
                "profession": '22'
            },
            json:true,
        };
        request.post(options, function(error,res,body) {
            expect(res.statusCode).to.equal(400);
            expect(res.body.err).to.equal('Bad data');
            done();
        });
    });

    it ('Has to throw 400 and send err:\"Bad data\" - no username has been given', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/createUser',
            body: {
                "userName": null,
                "profession": 'Director'
            },
            json:true,
        };
        request.post(options, function(error,res,body) {
            expect(res.statusCode).to.equal(400);
            expect(res.body.err).to.equal('Bad data');
            done();
        });
    });

    it ('Has to check increased total number of users', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/getUsers',
            json:true,
        };
        request.get(options, function(err,res,body) {
            expect(res.statusCode).to.equal(200);
            expect(res.body.result.length).to.equal(4);
            expect(Array.isArray(res.body.result)).to.equal(true);
            done();
        });
    });

    it ('Has to return user we\'ve just created', function(done) {
        var options = {
            url:'http://46.101.138.148:5640/getUser/4',
            json:true,
        };
        request.get(options, function(error,res,body) {
            expect(res.statusCode).to.equal(200);
            expect(res.body.result.userName).to.equal('PETRUHA');
            done();
        });
    });
});


describe('deleteUser', function() {
    it ('#Has to delete special user', function(done) {
        request.del({
            url:'http://46.101.138.148:5640/deleteUser/3',
            json:true
            },
            function(err,res,body) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.result).to.equal('User has been deleted');
                done();
            });
    });

    it ('#If no such user is exists', function(done) {
        request.del({
            url:'http://46.101.138.148:5640/deleteUser/3',
            json:true
            },
            function(err,res,body) {
                expect(res.statusCode).to.equal(400);
                expect(res.body.err).to.equal('Bad data');
                done();
            });
    });

    it ('#Has to check amount of remaining users after deleting', function(done) {
        request.get({
            url:'http://46.101.138.148:5640/getUsers',
            json:true
            },
            function(err, res, body) {
                expect(res.statusCode).to.equal(200);
                expect(res.body.result.length).to.equal(3);
                expect(Array.isArray(res.body.result)).to.equal(true);
                done();
             });
    });
});