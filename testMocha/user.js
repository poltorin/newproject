var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    id: String,
    userName: String,
    profession: String
});

module.exports = mongoose.model('User', UserSchema);